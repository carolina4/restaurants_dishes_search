from simstring.feature_extractor.character_ngram import CharacterNgramFeatureExtractor
from simstring.measure.cosine import CosineMeasure
from simstring.database.dict import DictDatabase
from simstring.searcher import Searcher

import json

from pymongo import MongoClient

# get db dishes data
client = MongoClient('localhost', 27017)
db = client.menus
collection = db.menu_search_restaurant
dishes_data = collection.find({}, {"plate_name": 1, "_id": 0})

# include plate words in simstring dict
db = DictDatabase(CharacterNgramFeatureExtractor(2))
for plate in dishes_data :
    total_words = plate["plate_name"].split()
    for word in total_words :
        db.add(word)


searcher = Searcher(db, CosineMeasure())
results = searcher.search('tuna', 0.8)


try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle

with open('dictionary.p', 'wb') as fp:
    pickle.dump({"db": db}, fp, protocol=pickle.HIGHEST_PROTOCOL)

'''
with open('dictionary.json', 'w+') as fp:
    json.dump(db, fp)
'''
