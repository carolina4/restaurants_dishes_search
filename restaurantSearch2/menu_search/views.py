# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext, loader
from django.shortcuts import render, get_object_or_404, render_to_response
import json
import operator
import re

from simstring.feature_extractor.character_ngram import CharacterNgramFeatureExtractor
from simstring.measure.cosine import CosineMeasure
from simstring.database.dict import DictDatabase
from simstring.searcher import Searcher
from pymongo import MongoClient

from django.views.decorators.csrf import csrf_exempt

from models import restaurant

from forms import DishNameForm

from calc_distance import calc_distance

#from cossine_similarity import get_cosine, text_to_vector


# variables
did_setup = False
searcher = []

def setup_similar_words():
    # get db dishes data
    client = MongoClient('localhost', 27017)
    db = client.menus
    collection = db.menu_search_restaurant
    dishes_data = collection.find({}, {"plate_name": 1, "_id": 0})

    # include plate words in simstring dict
    db = DictDatabase(CharacterNgramFeatureExtractor(2))
    for plate in dishes_data :
        total_words = plate["plate_name"].split()
        for word in total_words :
            db.add(word)

    searcher = Searcher(db, CosineMeasure())
    return searcher

def filter_repeated_restaurants(restaurants_list):
    MAX_RESTAURANTS_LIST = 10
    filtered_list = []
    known_restaurants = []
    for restaurant in restaurants_list:
        if not (restaurant.restaurant_name in known_restaurants):
            known_restaurants.append(restaurant.restaurant_name)
            filtered_list.append(restaurant)
    print(known_restaurants)
    return filtered_list[:MAX_RESTAURANTS_LIST]


def get_dish_name(request):
    global did_setup, searcher
    if did_setup == False:
        searcher = setup_similar_words()
        did_setup = True

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = DishNameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            dish_name = form.cleaned_data['dish_name']
            language = form.cleaned_data['language']
            latitude = form.cleaned_data['latitude']
            longitude = form.cleaned_data['longitude']
            order_by_distance = form.cleaned_data['order_by_distance']

            # misswritten words
            possible_plate_names = []
            dish_words = dish_name.split()
            for idx, val in enumerate(dish_words):
                similar_words = searcher.search(val, 0.8)
                for sim_word in similar_words:
                    possible_plate_name = dish_words
                    possible_plate_name[idx] = sim_word
                    possible_plate_name = " ".join(possible_plate_name)
                    possible_plate_names.append(".*"+possible_plate_name+".*")
            possible_plate_names = "|".join(possible_plate_names)

            #restaurants_list = restaurant.objects.raw_query( { "$and": [ { "plate_name": { "$regex": ".*"+dish_name+".*" , "$options":"i"} }, { "language": language } ] })
            restaurants_list = restaurant.objects.raw_query( { "$and": [ { "plate_name": re.compile(possible_plate_names,re.IGNORECASE) }, { "language": language } ] })

            #calculate distance
            if order_by_distance!=[]:
                order_by_distance = float(order_by_distance[0])
                if latitude=="" or longitude=="":
                    return render_to_response('menu_search/searchPage.html', {'form': form, 'restaurants_list': restaurants_list, 'error_message': 'WARNING: Unknown coordinates.'}, context_instance=RequestContext(request))
                latitude = float(latitude)
                longitude = float(longitude)          
                orig_coordinates = (latitude, longitude)

                restaurants_list = filter_repeated_restaurants(restaurants_list)            

                return_tuple = calc_distance(orig_coordinates, order_by_distance, restaurants_list) #[:10]
                restaurants_list = return_tuple[0]
                found_distances = return_tuple[1]
                return render_to_response('menu_search/searchPage.html', {'form': form, 'restaurants_list_distance': restaurants_list, 'latitude':latitude, 'longitude':longitude, 'found_distances':found_distances}, context_instance=RequestContext(request))
            
            # render restaurants list:
            return render_to_response('menu_search/searchPage.html', {'form': form, 'restaurants_list': restaurants_list}, context_instance=RequestContext(request))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = DishNameForm()

    return render_to_response('menu_search/searchPage.html', {'form': form}, context_instance=RequestContext(request))

    
@csrf_exempt
def auto_complete(request):
    if request.method == 'POST':
        json_data = json.loads(request.body) # request.raw_post_data w/ Django < 1.4
        try:
            input_text = json_data['input_text']
            language = json_data['language']
        except KeyError:
            return HttpResponseServerError("Malformed data!")
        complete_list = restaurant.objects.raw_query( { "$and": [ { "plate_name": { "$regex": "^"+input_text , "$options":"i"} }, { "language": language }] }).values('plate_name')
        complete_list = [elem["plate_name"] for elem in complete_list]
        complete_list = list(set(complete_list))
        auto_complete_data = {}
        auto_complete_data['complete_list'] = complete_list[:10]
        response = HttpResponse(json.dumps(auto_complete_data), mimetype='application/json; charset=utf-8')
        response['Access-Control-Allow-Origin'] = '*'
        return response


