from django import forms
from django.utils.safestring import mark_safe

#dish name search and language form
class DishNameForm(forms.Form):
    dish_name = forms.CharField(label='',
                                max_length=500, 
                                required=True,
                                widget = forms.TextInput(attrs = {
                                    'onkeyup' : "requestList(event);submitSearch(event);",
                                    'placeholder':'Type dish name'
                                    }
                                ))
    LANGUAGES = ( #select options
        ('en', 'English'),
        ('fr', 'Francais'),
        ('nd', 'Nederlands'),
    )
    language = forms.ChoiceField(label='Language:', 
                                choices=LANGUAGES, 
                                required=True)

    latitude = forms.CharField(label='',
                                max_length=50,
                                widget = forms.HiddenInput(),
                                required=False)

    longitude = forms.CharField(label='',
                                max_length=50, 
                                widget = forms.HiddenInput(),
                                required=False)

    order_by_distance = forms.MultipleChoiceField(label='Filter by distance:',
                                                widget = forms.CheckboxSelectMultiple,
                                                choices=[('1', '1Km'), ('5', '5Km')],
                                                required=False
                                                )
'''
    order_by_distance = forms.BooleanField(label='Filter by distance: 1Km',
                                            required=False,
                                            widget = forms.CheckboxInput(attrs = {
                                                'onchange' : "getLocation(event);",
                                                'value' : "1",
                                                'id' : "id_order_by_distance_1km"
                                                }
                                            ))

    order_by_distance = forms.BooleanField(label='5Km',
                                        required=False,
                                        widget = forms.CheckboxInput(attrs = {
                                            'onchange' : "getLocation(event);",
                                            'value' : "5",
                                            'id' : "id_order_by_distance_5km"
                                            }
                                        ))
    '''                                    



#how to display a list in the admin page
class ObjectListField(forms.CharField):
    def prepare_value(self, value):
        if isinstance(value, basestring):
            return value
        final_str = ""
        for entry in value:
            whours = ""
            for working_hours in entry["hours"]:
                whours = whours + " , " + working_hours
            final_str = final_str + entry["day"] + "->" + whours + " | "
        return final_str

    def to_python(self, value):
        if not value:
            return []
        final_obj = []
        lines = value.split(" | ")[:-1]
        for line in lines:
            day_hour = line.split("->")
            day = day_hour[0]
            hours = day_hour[1]
            individual_hour = hours.split(" , ")[1:]
            day_obj = {"day": day, "hours": []}
            for hour in individual_hour:
                day_obj["hours"].append(hour)
            final_obj.append(day_obj)
        return final_obj