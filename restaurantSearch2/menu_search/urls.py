from django.conf.urls import patterns, include, url
import views

from django.contrib import admin
admin.autodiscover()

#python manage.py runserver

urlpatterns = patterns('',
    url(r'^/autoComp/', views.auto_complete),
    url(r'^/$', views.get_dish_name, name='get_dish_name'),
    
    
    #url(r'^search/$', views.get_dish_name, name='get_dish_name'),
    
    

    # ex: /polls/5/vote/
    #url(r'^(?P<poll_id>\d+)/vote/$', views.vote, name='vote'),
)