from django.contrib import admin
from menu_search.models import restaurant

admin.site.register(restaurant)
'''para costumizar o admin, passar como parametro no register

#NO EDIT

#definir a ordem dos campos

class PollAdmin(admin.ModelAdmin):
    fields = ['pub_date', 'question']

admin.site.register(Poll, PollAdmin)

##########################################
#dividir o form em field sets

class PollAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date']}),
    ]

admin.site.register(Poll, PollAdmin)

##########################################
#campos inicialmente escondidos, collapse

class PollAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]

##########################################
#existe uma estrutura Choice que aparece na mesma pagina edit que o post. aparecem 3 escolhas

from django.contrib import admin
from polls.models import Choice, Poll

class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3

class PollAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]

admin.site.register(Poll, PollAdmin)

##########################################
#para os campos da choice aparecerem todos alinhados, trocar o cabecalho
class ChoiceInline(admin.StackedInline): -> class ChoiceInline(admin.TabularInline):

##########################################
#NO VIEW

# mostrar os campos da tabla
class PollAdmin(admin.ModelAdmin):
    # ...
    list_display = ('question', 'pub_date')

# adicionar filtros
class PollAdmin(admin.ModelAdmin):
    # ...
    list_filter = ['pub_date']      #filter by date 
    search_fields = ['question']    #search by question field
    date_hierarchy = 'pub_date'     #order by date
'''