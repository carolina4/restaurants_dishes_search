from django.db import models
from djangotoolbox.fields import ListField

from djangotoolbox.fields import EmbeddedModelField
from django_mongodb_engine.contrib import MongoDBManager

from .forms import ObjectListField


class CategoryField(ListField):
    def formfield(self, **kwargs):
        return models.Field.formfield(self, ObjectListField, **kwargs)

class restaurant(models.Model):
    restaurant_name = models.TextField()
    cusine_type = models.TextField()
    restaurant_address = models.TextField()
    restaurant_schedule = CategoryField() #ListField()
    plate_price = models.TextField()
    plate_name = models.TextField()
    plate_aditional_info = models.TextField()
    plate_description = models.TextField()
    language = models.TextField()

    objects = MongoDBManager()
