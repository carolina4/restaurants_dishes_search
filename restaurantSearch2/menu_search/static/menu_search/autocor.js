
const server_url = "http://localhost:8000/"

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

//var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
            xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
        }
    }
});


/////////////////////////////////////////////////////////////
/*
function submitSearchForm() {
  var url = server_url+'search/';
  var form_data = { latitude : document.getElementById("id_latitude").value,
                    longitude : document.getElementById("id_longitude").value,
                    dish_name : document.getElementById("id_dish_name").value,
                    language : document.getElementById("id_language").value,
                    order_by_distance : document.getElementById("id_order_by_distance_1Km").value,
                    order_by_distance : document.getElementById("id_order_by_distance_5Km").value                                        
  }
  $.ajax({
    type: "POST",
    url: url,
    data: JSON.stringify(form_data),
    dataType: 'text',
    success: function(data) {
       console.log('Well success');
    },
    failure: function(data) { 
        alert('Got an error dude');
    }
  });
  return false;
}
*/

/*
function print_map(){
  var latitude = 51.505;
  var longitude = -0.09;
  var zoom = 13;
  var mymap = L.map('mapid').setView([latitude, longitude], zoom);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiY2Fyb2xpbmE0NDQ0IiwiYSI6ImNqdDZ6dmI0NTBrdmw0NXJ1bHl6dmFtZ24ifQ.XWGS7CZ_21UbkdhUqlO0mA'
  }).addTo(mymap);

}
*/

function print_map(latitude,longitude,found_distances){
  var zoom = 13;
  var mymap = L.map('mapid').setView([latitude, longitude], zoom);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiY2Fyb2xpbmE0NDQ0IiwiYSI6ImNqdDZ6dmI0NTBrdmw0NXJ1bHl6dmFtZ24ifQ.XWGS7CZ_21UbkdhUqlO0mA'
  }).addTo(mymap);
  var restaurants_list = JSON.parse(found_distances);
  var markers = []
  for(idx=0; idx<restaurants_list.length; idx++) {
    var latitude = restaurants_list[idx]['latitude'];
    var longitude = restaurants_list[idx]['longitude'];
    marker = L.marker([latitude, longitude]).bindPopup("<b>"+restaurants_list[idx]['restaurant_name']+"</b>").openPopup().addTo(mymap);
    marker.on('mouseover', function (e) {
      this.openPopup();
    });
    marker.on('mouseout', function (e) {
      this.closePopup();
    });
    markers[idx] = marker
  }

}

function submitSearch(event){
  var x = event.charCode || event.keyCode;  // Get the Unicode value
  if(x=="13") { 
    event.preventDefault();
    document.getElementById("submit_button").click();
  }
}

var arr = [];
var last_event;

$( document ).ready(function() {
  autocomplete(document.getElementById("id_dish_name"));
});

function getGLock(){
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position){
      document.getElementById("id_latitude").value = position.coords.latitude;
      document.getElementById("id_longitude").value = position.coords.longitude;
    });
  } else {
    alert("Geolocation is not supported by this browser.");
  }  
}

function getLocation(event) {
  if(event.target.checked==false){
    document.getElementById("id_latitude").value = "";
    document.getElementById("id_longitude").value = "";
    return;
  }
  if(event.target.id=="id_order_by_distance_1Km"){
    document.getElementById("id_order_by_distance_5Km").checked = false;
  }
  if(event.target.id=="id_order_by_distance_5Km"){
    document.getElementById("id_order_by_distance_1Km").checked = false;
  }
  getGLock();
}

function requestList(event){
  var pressed_key_code = event.charCode || event.keyCode;
  if(pressed_key_code==40 || pressed_key_code==38 || pressed_key_code==13)
    return;
  var entered_input = document.getElementById("id_dish_name").value; 
  var language = document.getElementById("id_language").value;
  const url = server_url+'search/autoComp/';
  var typed_input = {input_text: entered_input, language: language};
  if(entered_input != ""){
      $.ajax({
          type: "POST",
          url: url,
          data: JSON.stringify(typed_input),
          dataType: 'text',
          success: function(data) {
              var data_list = JSON.parse(data);
              arr = data_list.complete_list;
              document.getElementById("id_dish_name").dispatchEvent(last_event);
          },
          failure: function(data) { 
              alert('Got an error dude');
          }
      });
  }
}


function autocomplete(inp) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function inputHandler(e) {
        last_event = e;
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("div");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
          }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function keyDownAutoCompHandler(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
} 

