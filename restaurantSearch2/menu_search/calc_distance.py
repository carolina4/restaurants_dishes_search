import networkx as nx
import osmnx as ox
import requests
import matplotlib.cm as cm
import matplotlib.colors as colors
import os
import matplotlib.pyplot as plt
from IPython.display import IFrame

import unidecode

#SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

os.environ["PROJ_LIB"] = "C:\Users\Carolina\Anaconda2\envs\py33\Library\share" #windows

ox.config(use_cache=True, log_console=True)
ox.__version__

def calc_distance(orig_coordinates, distance_from_point, restaurants_list):

    G = ox.graph_from_point(orig_coordinates, distance=distance_from_point*1000, distance_type="network", network_type="drive", simplify=False)
    orig_node = ox.get_nearest_node(G, orig_coordinates)
    restaurants_list_distance = []
    found_distances = []

    for restaurant in restaurants_list:
        # how long is our route in meters?
        try:
            address = restaurant.restaurant_address
            #check if alleready calculated
            distance = next((x['distance'] for x in found_distances if x['address'] == address), -1)
            if  distance == -1 :
                dest_coordinates = ox.utils.geocode(unidecode.unidecode(address))
                print(dest_coordinates)
                dest_node = ox.get_nearest_node(G, dest_coordinates)
                distance = nx.shortest_path_length(G, orig_node, dest_node, weight='length')*0.001
                found_distances.append({'address': address, 'distance': distance, 'latitude': dest_coordinates[0], 'longitude': dest_coordinates[1], 'restaurant_name': restaurant.restaurant_name})
        except Exception as e:
            print(e)
        else:
            #save route if not exception
            setattr(restaurant, 'distance', distance)
            restaurants_list_distance.append(restaurant)

    #graph_map = ox.plot_graph_folium(G, popup_attribute='name', tiles='stamenterrain', zoom=3, fit_bounds=True, edge_width=2, edge_opacity=0)
    #filepath = os.path.join(SITE_ROOT, "static/menu_search/graph.html").replace('\\', '/')
    #graph_map.save(filepath)
    #IFrame(filepath, width=600, height=500)

    #fig, ax = ox.plot_graph(G, show=False, close=False)
    #ax.scatter(4.3650759, 50.84975850000001, c='red') #longitude, latitude
    #ax.scatter(4.37, 50.84975850000001, c='blue')
    #plt.show()

    return sorted(restaurants_list_distance, key=lambda x: x.distance), found_distances
