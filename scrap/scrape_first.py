# import libraries
import urllib3
from bs4 import BeautifulSoup

# specify the url
quote_page = 'http://www.bloomberg.com/quote/SPX:IND'

# query the website and return the html to the variable ‘page’
http = urllib3.PoolManager()
r = http.request('GET', 'http://www.bloomberg.com/quote/SPX:IND')
page = r.data

# parse the html using beautiful soup and store in variable `soup`
soup = BeautifulSoup(page, 'html.parser')

# Take out the <div> of name and get its value
#name_box = soup.find('span', attrs={'class': 'companyId__87e50d5a'})
name_box = soup.find('title')
name = name_box.text.strip() # strip() is used to remove starting and trailing 

# get the index price
price_box = soup.find('span', attrs={'class':'priceText__1853e8a5'})
price = price_box.text
print(page)