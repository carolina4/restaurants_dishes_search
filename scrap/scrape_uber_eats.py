# import libraries
#import urllib3
import requests
from bs4 import BeautifulSoup
import spynner

browser = spynner.Browser() 
browser.load("http://www.wordreference.com") 
browser.runjs("console.log('I can run Javascript!')") 
browser.runjs("_jQuery('div').css('border', 'solid red')") # and jQuery! 
browser.select("#esen") 
browser.fill("input[name=enit]", "hola") 
browser.click("input[name=b]") 
browser.wait_load() 
print(browser.url)
len(browser.html) 
browser.close() 


# specify the url
quote_page = 'https://www.ubereats.com/en-BE/stores/'

# query the website and return the html to the variable ‘page’
#http = urllib3.PoolManager()
hdr = { 'Remote address' : '104.36.195.165:443',
        'Version' : 'HTTP/1.1',
        'Referrer Policy' : 'no-referrer-when-downgrade'}
cookies = {'_cc'	: 'AUQAmJrPUBhe5RZwB90oQ8/L',
'_fbp' :	'fb.1.1550149125935.1017770625',
'_ga' : 'GA1.2.1188886808.1550149126',
'_gcl_au' :	'1.1.869248139.1550149125',
'_gid' :	'GA1.2.2031262676.1550149126',
'_ua2' :	'{"id":"64597f28-0068-413a-bdf9-7d18692926b0","ts":1550149120885,"counter":24}',
'_userUuid' :	'undefined',
'aam_uuid' :	'72130960772192850911487354571993366810',
'AAMC_uber_0' :	'AMSYNCSOP|411-17949',
'AMCV_0FEC8C3E55DB4B027F000101@AdobeOrg' :	'1611084164|MCMID|72537315583320812761446882379362741509|MCAAMLH-1550753925|6|MCAAMB-1550753925|6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y|MCOPTOUT-1550156325s|NONE|MCSYNCSOP|411-17949',
'AMCVS_0FEC8C3E55DB4B027F000101@AdobeOrg' :	'1',
'marketing_vistor_id'	: 'eaf5f83a-919a-4972-aa5f-9dd8134d77b8',
'mcd_restaurant' :	'McDonald\'s - Ixelles',
'ubereats.analyticsOptIn' :	'true',
'ubereats.cart.time' :	'{"targetDeliveryTimeRange":\{\}}',
'ubereats.cartLocation.location' :	'{"location":{"loaded":true,"loading":false,"requestedAt":1550149147419,"data":{"latitude":50.849458,"longitude":4.3666482,"reference":"EiRDb25ncmVzc3RyYWF0LCAxMDAwIEJydXNzZWwsIEJlbGdpdW0iLiosChQKEglvVS_IfcPDRxFeUo6AyrEGYxIUChIJZ2jHc-2kw0cRpwJzeGY6i8E","type":"google_places","address":{"title":"Congresstraat","address1":"Congresstraat","city":"Brussel"}},"successAt":1550149148010}}',
'udi-fingerprint' :	'NABdFEoWeEUpCkLTERT/gbdmrZ4eoIdUL8ObLpapNrZ7pXSH+wvIGqx7sep9Rr2kDKv7+chhEGnWxtIPmM2Rbw==k7+C6D+IHsXd3/Ga6NDRHPdMMu878J8gfXJ4AjAQMQc=',
'udi-id' :	'5Fh0DwP3Cn9l/Kn65zkHymsgL315jEPoArHHELDxMcQo5treiSUfMwC2SFGtnjeZuhOrCi+/PmuUNlV/b8QU+SntOXcyl4AMpkXYN+OBPqdtAsutJlDvhPV7FobsWjbtrAHBh/THDiumzs+qxMbZet6LbKxdJkTL0Y1TNW8PcwO7BY7nyw2O9C4zDoj49kQSB7ykiBhRVJRt/O15gOCrIw==9qog8z0MyFOP3Q73Jm2ydA==yHEDYV1gHdVSCAYFprrdwzDuW4w7uiN/eS0j78FSXr0=',
'utag_main' :	'v_id:0168ec168223001b5f6c63bf52210004e001f00d00978$_sn:1$_se:13$_ss:0$_st:1550151285848$ses_id:1550149124645;exp-session$_pn:2;exp-session',
'web-eats:sess' :	'FCxTLaewQKMSwwck0_dfyw.Vapq3V4pe6sc_DBdVJe5g-RH8jgluPRUagh34AXJ3AlPvh3YIcG53NA0gd9GZ1aXm6VkGv0Qu6LvHJVaNe2VacJ0T5rtZiN4rHHh34jbpFP0eWsUPReYfHLVmBPVqJ5VgiUr-CwwLxI2robHwkkwpB32M1-xCd4nJI7OCXWc3krrbUTpj2FQ-dc5veghIeCS9jhojup6UFtNZLpIdR1YS9cEoBWj-yn8WzBGi76ZOoz9dUsWQIauzFCZqsT3lP4DGfIja1iQBaPDf-KGmHSm2g.1550149119788.1209600000.FWvMiavjoRZKCL5fmkiJ1BXIugvy84C5FFFja-letos',
'xpId' :	'{"uuid":"3eb8ae8b-8d83-4796-9e38-845bedceb05a"}'
}
#r = http.request('GET', quote_page, headers=hdr, cookies)
r = requests.get(quote_page, headers=hdr, cookies=cookies)
page = r.text

##########################################
#f= open("guru99.txt","w+")
#f.write(page.encode("unicode"))
#f.close()
print(page)
##########################################


# parse the html using beautiful soup and store in variable `soup`
soup = BeautifulSoup(page, 'html.parser')

# Take out the <div> of name and get its value
#name_box = soup.find('span', attrs={'class': 'companyId__87e50d5a'})
all_restaurants = soup.find_all('div', attrs={'style': 'display: flex; flex-direction: column; width: 33.3333%; padding-left: 32px;'})
print(all_restaurants)
restaurant = all_restaurants.pop() # strip() is used to remove starting and trailing 
print(restaurant)


# get the index price
price_box = soup.find('span', attrs={'class':'priceText__1853e8a5'})
price = price_box.text
print(page)