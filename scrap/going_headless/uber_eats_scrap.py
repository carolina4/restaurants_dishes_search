import os  
from selenium import webdriver  
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.chrome.options import Options  
from selenium.common.exceptions import NoSuchElementException, TimeoutException

import urllib3
from bs4 import BeautifulSoup

from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import pymongo

import io

#create database
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["restaurants_info"]
mycol = mydb["menu"]
mycol.delete_many({})

#initiate chrome driver
chrome_options = Options()  
#chrome_options.add_argument("--headless")  
chrome_options.binary_location = 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe'   

#start chrome driver process, chrome driver must be in the same folder
driver = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"),   chrome_options=chrome_options)  
driver.get("https://www.ubereats.com/en-BE/")

#enter destination address
address_bar = driver.find_element_by_xpath("//*[@id=\"address-selection-input\"]") # driver.find_element_by_class_name('btn btn--sm btn--outline external')  
address_bar.clear()
address_bar.send_keys("Rue du Congrès")

#select in the dropdown menu
driver.implicitly_wait(3)
drop_down_buttton = driver.find_element_by_xpath("//*[contains(text(),'Rue du Congrès')]")
drop_down_buttton.click()

#get all restaurant links
WebDriverWait(driver, 40).until(
    EC.visibility_of_all_elements_located((By.XPATH, '//div[@style="margin-left: -32px; margin-bottom: 32px;"]/div[@style="display: flex; flex-direction: column; width: 33.3333%; padding-left: 32px;"]/a[@style="display: block;"]'))
)

restaurants_link_list = []

#scroll to get complete view of the html page
SCROLL_PAUSE_TIME = 3

# Get scroll height
last_height = driver.execute_script("return document.body.scrollHeight")

SCROLL_STEPS = list(range(0, last_height, 100))

for step in SCROLL_STEPS:
    # Scroll down to bottom
    driver.execute_script("window.scrollTo(0, "+str(step)+");")

    # Wait to load page
    #sleep(SCROLL_PAUSE_TIME)
    WebDriverWait(driver, 40).until(
        EC.visibility_of_all_elements_located((By.XPATH, '//div[@style="margin-left: -32px; margin-bottom: 32px;"]/div[@style="display: flex; flex-direction: column; width: 33.3333%; padding-left: 32px;"]/a[@style="display: block;"]'))
    )

    #get html list
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    found_restaurants = soup.find_all('a', attrs={'style': 'display: block;'})
    for restaurant in found_restaurants :
        if not(any(known_restaurant.text == restaurant.text for known_restaurant in restaurants_link_list)) :
            restaurants_link_list.append(restaurant)
            #variables
            restaurant_name = restaurant.contents[0].contents[1].text
            cusine_type = restaurant.contents[0].contents[2].contents[0].text
            cusine_type = cusine_type.split("€ • ")[1]
            delivery_time = restaurant.contents[0].contents[2].contents[1].contents[0].contents[0].text
            restaurant_rate = restaurant.contents[0].contents[2].contents[1].contents[0].contents[1].text
            restaurant_rate = restaurant_rate.split("  (200+)")[0]
            #get restaurant address
            driver_address = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"),   chrome_options=chrome_options)  
            driver_address.get("https://www.google.com/maps/")
            main_search = driver_address.find_element_by_xpath('//input[@id="searchboxinput"]')
            main_search.send_keys(restaurant_name+" restaurant")
            main_search.send_keys(Keys.ENTER)
            driver_address.implicitly_wait(3)

            try: #if there is only one result
                WebDriverWait(driver_address, 40).until(
                   EC.visibility_of_any_elements_located((By.XPATH, '//span[@class="section-info-text"]'))
                )
                soup_address = BeautifulSoup(driver_address.page_source, 'html.parser')
                found_info = soup_address.find_all('span', attrs={'class': 'section-info-text'})
                #address
                address_icones = soup_address.find_all('span', attrs={'class': 'section-info-icon maps-sprite-pane-info-address'})
                if len(address_icones)==1:
                    address_nd = [address_icones[0].parent.contents[7].text]
                    address_fr = [" "]
                elif len(address_icones)==2:
                    address_nd = [address_icones[0].parent.contents[7].text]
                    address_fr = [address_icones[1].parent.contents[7].text]
                else:
                    print("Problem with localizations array in restaurant "+restaurant_name)

                if(restaurant_name=="Pizza Oliva Etterbeek" or restaurant_name=="Break Point"):
                        print("AQUIIIII")

                #schedule
                found_schedule_info = soup_address.find('span', attrs={'class': 'section-info-hour-text'})
                schedule = [[]]
                #dismiss checkbox
                try:
                    remind_me_latter_button = driver_address.find_element_by_xpath('//button[@class="widget-consent-button-later ripple-container"]')
                    remind_me_latter_button.click()
                except NoSuchElementException as e:
                    print("No privacy box")
                try:
                    if (found_schedule_info.parent.parent.parent.previous_sibling.attrs['class'][0]=='section-subheader' or  
                                found_schedule_info.parent.parent.parent.previous_sibling.previous_sibling.attrs['class'][0]=='section-subheader'):
                        print("No schedule info for restaurant "+restaurant_name)
                    else :
                        raise Exception()
                except (AttributeError,Exception) as e:
                    WebDriverWait(driver_address, 40).until(
                        EC.visibility_of_element_located((By.XPATH, '//div[@class="section-info-line section-info-hoverable"]'))
                    )
                    schedule_button = driver_address.find_element_by_xpath('//div[@class="section-info-line section-info-hoverable"]')
                    schedule_button.click()
                    WebDriverWait(driver_address, 40).until(
                        EC.visibility_of_any_elements_located((By.XPATH, '//table[@class="widget-pane-info-open-hours-row-table-hoverable"]'))
                    )
                    schedule_table = soup_address.find('table', attrs={'class': 'widget-pane-info-open-hours-row-table-hoverable'})             
                    for week_day in range(len(schedule_table.contents[1].contents)-1):
                        schedule[0].append({"day": schedule_table.contents[1].contents[week_day].contents[1].text,
                                           "hours": schedule_table.contents[1].contents[week_day].contents[3].contents[1].text})
                
                #phone
                found_phone_info = soup_address.find('span', attrs={'class': 'section-info-icon maps-sprite-pane-info-phone'})
                try:
                    if (found_phone_info.parent.parent.parent.previous_sibling.attrs['class'][0]=='section-subheader' or  
                                found_phone_info.parent.parent.parent.previous_sibling.previous_sibling.attrs['class'][0]=='section-subheader'):
                        print("No phone info for restaurant "+restaurant_name)
                        phone_number = [" "]
                    else :
                        raise Exception()
                except (AttributeError,Exception) as e:
                    phone_number = [found_phone_info.parent.contents[7].text]

            except (NoSuchElementException, TimeoutException) as e:
                found_address_restaurants = driver_address.find_elements_by_xpath('//div[@class="section-result-content"]')
                soup_address = BeautifulSoup(driver_address.page_source, 'html.parser')
                found_address_names = soup_address.find_all('h3', attrs={'class': 'section-result-title'})
                address_nd = []
                address_fr = []
                schedule = [[] for i in range(len(found_address_restaurants))]
                phone_number = []
                for idx in range(len(found_address_restaurants)):
                    cur_name = found_address_names[idx].text
                    if cur_name.upper().replace(" ", "")==restaurant_name.upper().replace(" ", "") : 
                        found_address = found_address_restaurants[idx]
                        found_address.click()
                        WebDriverWait(driver_address, 200).until(
                            EC.visibility_of_any_elements_located((By.XPATH, '//span[@class="section-info-text"]'))
                        )
                        soup_address = BeautifulSoup(driver_address.page_source, 'html.parser')
                        found_info = soup_address.find_all('span', attrs={'class': 'section-info-text'})
                        #address
                        address_icones = soup_address.find_all('span', attrs={'class': 'section-info-icon maps-sprite-pane-info-address'})
                        if len(address_icones)==1:
                            address_nd.append(address_icones[0].parent.contents[7].text)
                            address_fr.append([" "])
                        elif len(address_icones)==2:
                            address_nd.append(address_icones[0].parent.contents[7].text)
                            address_fr.append(address_icones[1].parent.contents[7].text)
                        else:
                            print("Problem with localizations array in restaurant "+restaurant_name)

                        if(restaurant_name=="Pizza Oliva Etterbeek" or restaurant_name=="Break Point" or restaurant_name=="Breakpoint"):
                            print("AQUIIIII")

                        #schedule
                        found_schedule_info = soup_address.find('span', attrs={'class': 'section-info-icon maps-sprite-pane-info-hours'})
                        try:
                            if (found_schedule_info.parent.parent.parent.previous_sibling.attrs['class'][0]=='section-subheader' or  
                                    found_schedule_info.parent.parent.parent.previous_sibling.previous_sibling.attrs['class'][0]=='section-subheader'):
                                print("No schedule info for restaurant "+restaurant_name)
                            else :
                                raise Exception() 
                        except (AttributeError,Exception) as e:
                            WebDriverWait(driver_address, 40).until(
                                EC.visibility_of_element_located((By.XPATH, '//div[@class="section-info-line section-info-hoverable"]'))
                            )
                            #dismiss checkbox
                            try:
                                remind_me_latter_button = driver_address.find_element_by_xpath('//button[@class="widget-consent-button-later ripple-container"]')
                                remind_me_latter_button.click()
                            except NoSuchElementException as e:
                                print("No privacy box")
                            schedule_button = driver_address.find_element_by_xpath('//div[@class="section-info-line section-info-hoverable"]')
                            schedule_button.click()
                            WebDriverWait(driver_address, 200).until(
                                EC.visibility_of_any_elements_located((By.XPATH, '//table[@class="widget-pane-info-open-hours-row-table-hoverable"]'))
                            )
                            schedule_table = soup_address.find('table', attrs={'class': 'widget-pane-info-open-hours-row-table-hoverable'}) 
                            for week_day in range(len(schedule_table.contents[1].contents)-1):
                                schedule[idx].append({"day": schedule_table.contents[1].contents[week_day].contents[1].text,
                                                        "hours": schedule_table.contents[1].contents[week_day].contents[3].contents[1].text})
                        #phone
                        found_phone_info = soup_address.find('span', attrs={'class': 'section-info-icon maps-sprite-pane-info-phone'}) 
                        try:
                            if (found_phone_info.parent.parent.parent.previous_sibling.attrs['class'][0]=='section-subheader' or  
                                    found_phone_info.parent.parent.parent.previous_sibling.previous_sibling.attrs['class'][0]=='section-subheader'):
                                print("No phone info for restaurant "+restaurant_name)
                                phone_number.append(" ")
                            else :
                                raise Exception()
                        except (AttributeError,Exception) as e:
                            phone_number.append(found_phone_info.parent.contents[7].text)

                        search_button = driver_address.find_element_by_id("searchbox-searchbutton")
                        search_button.click()
                        WebDriverWait(driver_address, 200).until(
                            EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="section-result-content"]'))
                        )
                        found_address_restaurants = driver_address.find_elements_by_xpath('//div[@class="section-result-content"]')
                    else :
                        schedule.pop()
                print("Many options for restaurant: "+restaurant_name)
            driver_address.close()

            #get into the restaurant site
            restaurant_button = driver.find_element_by_xpath('//a[@href="'+restaurant.attrs['href']+'"]')
            restaurant_button.click()
            WebDriverWait(driver, 40).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//div[@style="margin-left: -24px;"]'))
            )
            soup_article = BeautifulSoup(driver.page_source, 'html.parser')
            found_articles = soup_article.find_all('div', attrs={'style': 'margin-left: -24px;'})
            for article in found_articles :
                #variables
                article_name = article.contents[0].contents[0].contents[0].contents[0].text
                article_description = article.contents[0].contents[0].contents[0].contents[1].text
                article_price = article.contents[0].contents[0].contents[0].contents[2].contents[0].text
                article_price = article_price.split("EUR")[1][1:]
                #save into the database
                for idx in range(len(phone_number)) :
                    data = { "restaurant_name": restaurant_name, "cusine_type": cusine_type, "delivery_time": delivery_time,
                    "restaurant_rate_uber_eats": restaurant_rate, "address_nd": address_nd[idx], "address_fr": address_fr[idx],
                    "phone_number": phone_number[idx], "schedule": schedule[idx], 
                    "article_name": article_name, "article_description": article_description, "article_price": article_price}
                    mycol.insert_one(data)
            driver.execute_script("window.history.go(-1)")

driver.close()
myclient.close()

#with io.open("guru99.html", "w+", encoding="utf-8") as f:
#    f.write(page)
#f.close() 

