import os  
from selenium import webdriver  
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.chrome.options import Options  
from selenium.common.exceptions import NoSuchElementException, TimeoutException

import urllib3
from bs4 import BeautifulSoup

from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import pymongo

INITIAL_LOCALIZATION = "Rue du Congrès, Brussels"


#FUNCTIONS######################################################
def scratch_restaurant_menu(driver, mycol, cusine_type, restaurant_name, restaurant_address, restaurant_schedule):
    #go back to menu page
    goTo_menu_button = driver.find_element_by_xpath('//li[@id="tab_MenuCard"]')
    goTo_menu_button.click()
    WebDriverWait(driver, 200).until(
        EC.presence_of_all_elements_located((By.XPATH, '//div[@class="meal"]'))
    )
    #find info in page
    soup_menus = BeautifulSoup(driver.page_source, 'html.parser')
    found_infos = soup_menus.find_all('div', attrs={'class': 'meal'})
    for found_info, idx in zip(found_infos, range(0,len(found_infos))):
        #extract info
        plate_price = found_info.findChild('span', attrs={'itemprop': 'price'}).text

        plate_name = found_info.findChild('span', attrs={'class': 'meal-name'}).text
        plate_name = plate_name.replace("\n ","")
        plate_name = plate_name.strip()

        found_aditional_info = found_info.findChild('div', attrs={'class': 'meal-description-additional-info'})
        if found_aditional_info!=None:
            plate_aditional_info = found_aditional_info.text
        else:
            plate_aditional_info = ""

        found_meal_description = found_info.findChild('div',attrs={'class': 'meal-description-choose-from'})
        if found_meal_description!=None:
            plate_description = found_meal_description.text
        else:
            plate_description = ""
        
        data = { "restaurant_name": restaurant_name, "cusine_type": cusine_type, "restaurant_address": restaurant_address,
                    "restaurant_schedule": restaurant_schedule, "plate_price": plate_price, "plate_name": plate_name,
                    "plate_aditional_info": plate_aditional_info, "plate_description": plate_description}
        mycol.insert_one(data)


def get_schedule(driver):
    #find schedule info
    soup_schedule = BeautifulSoup(driver.page_source, 'html.parser')
    found_info = soup_schedule.find('i', attrs={'class': 'icon-ta-delivery-time'})
    if found_info!=None:
        found_info = found_info.parent.parent.contents[3].contents[8].contents[1].contents
        schedule = [{'day': '', 'hours': []}] * 7
        for idx in range(0,len(found_info),2):
            #correct day string
            week_day = found_info[idx].contents[1].text
            week_day = week_day.replace("\n ","")
            week_day = week_day.strip()
            #build hours array
            hours_array = []
            for idx_hours in range(0,len(found_info[idx].contents[3].contents),2):
                hours_array.append(found_info[idx].contents[3].contents[idx_hours])
            #assign schedule day
            schedule[int(idx/2)] = {'day': week_day, 'hours': hours_array}
        return schedule
    else:
        return []


def get_address(driver):
    #go into page
    more_info_button = driver.find_element_by_xpath('//li[@id="tab_MoreInfo"]')
    more_info_button.click()
    WebDriverWait(driver, 200).until(
        EC.presence_of_element_located((By.XPATH, '//div[@class="infoBox-inner-container"]'))
    )
    #find address info
    soup_address = BeautifulSoup(driver.page_source, 'html.parser')
    found_info = soup_address.find('div', attrs={'class': 'infoBox-inner-container'})
    if len(found_info.contents[3].contents)==3:
        return (found_info.contents[3].contents[0][2:]+", "+found_info.contents[3].contents[2]).strip()
    else:
        return " "


def scratch_restaurant_page(driver, mycol, cusine_type, restaurant_name):
    #address
    restaurant_address = get_address(driver)
    #schedule
    restaurant_schedule = get_schedule(driver)
    #menu
    scratch_restaurant_menu(driver, mycol, cusine_type, restaurant_name, restaurant_address, restaurant_schedule)


def scratch_restaurants(driver, mycol):
    # Get restaurants list from page
    restaurants_list = driver.find_elements_by_xpath('//div[@class="restaurant grid"]')
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    found_info = soup.find_all('div', attrs={'class': 'restaurant grid'})

    # For each restaurant, get info
    for idx in range(len(restaurants_list)):
        restaurant_element = restaurants_list[idx]
        if restaurant_element.is_displayed():
            
            #Get cusine type
            cusine_type = found_info[idx].findChild('div', attrs={'class': 'kitchens'}).text
            cusine_type = cusine_type.replace('\n','')
            #Get restaurant name
            restaurant_name = (found_info[idx].findChild('h2', attrs={'class': 'restaurantname'}).text).strip()

            restaurant_element.click()

            #Wait until restaurant page is loaded
            WebDriverWait(driver, 200).until(
                EC.presence_of_element_located((By.XPATH, '//div[@id="menu-tab-content"]'))
            )
            scratch_restaurant_page(driver, mycol, cusine_type, restaurant_name)

            #Go back to restaurants list page and wait until its loaded
            back_button = driver.find_element_by_xpath('//div[@class="btn btn-back"]')
            back_button.click()
            WebDriverWait(driver, 200).until(
                EC.presence_of_all_elements_located((By.XPATH, '//div[@class="restaurant grid"]'))
            )
            #update restaurants_list
            restaurants_list = driver.find_elements_by_xpath('//div[@class="restaurant grid"]')
            

def handle_initial_page(driver, initial_localization):
    # Wait to load page
    WebDriverWait(driver, 200).until(
        EC.visibility_of_all_elements_located((By.XPATH, '//input[@id="imysearchstring"]'))
    )
    # Click on input and type destination
    search_input = driver.find_element_by_xpath('//input[@id="imysearchstring"]')
    search_input.click()
    search_input.send_keys(initial_localization)
    # Select first option on dropdown menu
    WebDriverWait(driver, 200).until(
        EC.presence_of_element_located((By.XPATH, '//a[@class="item selected"]'))
    )
    first_drop_down_op = driver.find_element_by_xpath('//a[@class="item selected"]')
    first_drop_down_op.click()
    #Wait until page is loaded
    WebDriverWait(driver, 200).until(
        EC.visibility_of_any_elements_located((By.XPATH, '//div[@class="restaurant grid"]'))
    )

#####################################################################

#create database
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["restaurants_info"]
mycol = mydb["menu_take_away"]
mycol.delete_many({})

#initiate chrome driver
chrome_options = Options()  
#chrome_options.add_argument("--headless")  
chrome_options.binary_location = 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe'   

#start chrome driver process, chrome driver must be in the same folder
driver = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"),   chrome_options=chrome_options)  
driver.get("https://www.takeaway.com/")

#input delivery localization, see the restaurants
handle_initial_page(driver, INITIAL_LOCALIZATION)

scratch_restaurants(driver,mycol)

driver.close()
driver.quit()




